package com.yzq.os.spider.v.util;

import java.io.UnsupportedEncodingException;

/**
 * 编码转换工具类
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public final class EncodeUtil {
	
	private EncodeUtil(){}
	
	/**
	 * UTF8编码字符串转换成ISO8859-1字符串
	 * @param input
	 * @return
	 */
	public static String utf2iso(String input) {
		String outPut = null;
		if (input == null) {
			return outPut;
		}
		try {
			outPut = new String(input.getBytes("UTF-8"), "ISO-8859-1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return outPut;
	}

	/**
	 * GBK编码字符串转换成ISO8859-1字符串
	 * @param input
	 * @return
	 */
	public static String gbk2iso(String input) {
		String outPut = null;
		if (input == null) {
			return outPut;
		}
		try {
			outPut = new String(input.getBytes("GBK"), "ISO-8859-1");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return outPut;
	}

	/**
	 * ISO8859-1编码字符串转换成UTF-8字符串
	 * @param input
	 * @return
	 */
	public static String iso2utf(String input) {
		String outPut = null;
		if (input == null) {
			return outPut;
		}
		try {
			outPut = new String(input.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return outPut;
	}

	/**
	 * GBK编码字符串转换成UTF-8字符串
	 * @param input
	 * @return
	 */
	public static String gbk2utf(String input) {
		String outPut = null;
		if (input == null) {
			return outPut;
		}
		try {
			outPut = new String(input.getBytes("GBK"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return outPut;
	}

	/**
	 * ISO8859-1编码字符串转换成GBK字符串
	 * @param input
	 * @return
	 */
	public static String iso2gbk(String input) {
		String outPut = null;
		if (input == null) {
			return outPut;
		}
		try {
			outPut = new String(input.getBytes("ISO-8859-1"), "GBK");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return outPut;
	}

	/**
	 * UTF-8编码字符串转换成GBK字符串
	 * @param input
	 * @return
	 */
	public static String utf2gbk(String input) {
		String outPut = null;
		if (input == null) {
			return outPut;
		}
		try {
			outPut = new String(input.getBytes("UTF-8"), "GBK");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return outPut;
	}
}

package com.yzq.os.spider.v.domain;

/**
 * 列表页面配置，配置网站列表的属性信息，例如每页显示数量，获取多少记录数和总数的正则表达式，等内容
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
public class ListPageConfig {
	private Integer id;
	/**
	 * 网站ID
	 */
	private Integer websiteId;
	/**
	 * 搜索引擎ID
	 */
	private int searchEngineId;
	/**
	 * 没有找到数据的页面正则
	 */
	private String noDataPageRegex;
	/**
	 * 返回记录总数的正则
	 */
	private String returnRecordNumRegex;
	/**
	 * 分页参数名称
	 */
	private String currentPageNoName;
	/**
	 * 当前页码
	 */
	private Integer pageSize;
	/**
	 * 列表最大显示记录数量，返回记录数如果超过将自动拼入其它条件，从而减少结果集
	 */
	private Integer maxRecordNum;
	/**
	 * 数据区域正则，默认为整个页面
	 */
	private String dataRegionRegex;
	/**
	 * 记录标题抽取正则
	 */
	private String jobTitleRegex;
	/**
	 * 记录的连接地址抽取正则
	 */
	private String jobHrefRegex;
	/**
	 * 记录日期数据抽取正则
	 */
	private String jobDateRegex;
	/**
	 * 记录日期的解析格式
	 */
	private String jobDatePattern;
	/**
	 * 记录城市抽取正则
	 */
	private String jobCityRegex;
	/**
	 * 记录类型名称(搜索引擎参数的一个，数据保存时会记录)
	 */
	private String jobTypeName;
	/**
	 * 记录类型名称(搜索引擎参数的一个，数据保存时会记录)
	 */
	private String industryName;
	/**
	 * 记录城市名称
	 */
	private String cityName;
	/**
	 * 记录发布公司名称
	 */
	private String companyNameRegex;
	/**
	 * 记录发布公司的连接地址
	 */
	private String companyHrefRegex;
	/**
	 * 网站名称
	 */
	private String websiteName;
	/**
	 * 搜索引擎名称
	 */
	private String searchEngineName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getSearchEngineId() {
		return searchEngineId;
	}

	public void setSearchEngineId(int searchEngineId) {
		this.searchEngineId = searchEngineId;
	}

	public String getNoDataPageRegex() {
		return noDataPageRegex;
	}

	public void setNoDataPageRegex(String noDataPageRegex) {
		this.noDataPageRegex = noDataPageRegex;
	}

	public String getReturnRecordNumRegex() {
		return returnRecordNumRegex;
	}

	public void setReturnRecordNumRegex(String returnRecordNumRegex) {
		this.returnRecordNumRegex = returnRecordNumRegex;
	}

	public String getCurrentPageNoName() {
		return currentPageNoName;
	}

	public void setCurrentPageNoName(String currentPageNoName) {
		this.currentPageNoName = currentPageNoName;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Integer getMaxRecordNum() {
		return maxRecordNum;
	}

	public void setMaxRecordNum(Integer maxRecordNum) {
		this.maxRecordNum = maxRecordNum;
	}

	public String getDataRegionRegex() {
		return dataRegionRegex;
	}

	public void setDataRegionRegex(String dataRegionRegex) {
		this.dataRegionRegex = dataRegionRegex;
	}

	public String getJobTitleRegex() {
		return jobTitleRegex;
	}

	public void setJobTitleRegex(String jobTitleRegex) {
		this.jobTitleRegex = jobTitleRegex;
	}

	public String getJobHrefRegex() {
		return jobHrefRegex;
	}

	public void setJobHrefRegex(String jobHrefRegex) {
		this.jobHrefRegex = jobHrefRegex;
	}

	public String getJobDateRegex() {
		return jobDateRegex;
	}

	public void setJobDateRegex(String jobDateRegex) {
		this.jobDateRegex = jobDateRegex;
	}

	public String getJobDatePattern() {
		return jobDatePattern;
	}

	public void setJobDatePattern(String jobDatePattern) {
		this.jobDatePattern = jobDatePattern;
	}

	public String getJobCityRegex() {
		return jobCityRegex;
	}

	public void setJobCityRegex(String jobCityRegex) {
		this.jobCityRegex = jobCityRegex;
	}

	public String getJobTypeName() {
		return jobTypeName;
	}

	public void setJobTypeName(String jobTypeName) {
		this.jobTypeName = jobTypeName;
	}

	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCompanyNameRegex() {
		return companyNameRegex;
	}

	public void setCompanyNameRegex(String companyNameRegex) {
		this.companyNameRegex = companyNameRegex;
	}

	public String getCompanyHrefRegex() {
		return companyHrefRegex;
	}

	public void setCompanyHrefRegex(String companyHrefRegex) {
		this.companyHrefRegex = companyHrefRegex;
	}

	public Integer getWebsiteId() {
		return websiteId;
	}

	public void setWebsiteId(Integer websiteId) {
		this.websiteId = websiteId;
	}

	public String getWebsiteName() {
		return websiteName;
	}

	public void setWebsiteName(String websiteName) {
		this.websiteName = websiteName;
	}

	public String getSearchEngineName() {
		return searchEngineName;
	}

	public void setSearchEngineName(String searchEngineName) {
		this.searchEngineName = searchEngineName;
	}

	@Override
	public String toString() {
		return "ListPageConfig [id=" + id + ", searchEngineId="
				+ searchEngineId + ", noDataPageRegex=" + noDataPageRegex
				+ ", returnRecordNumRegex=" + returnRecordNumRegex
				+ ", currentPageNoName=" + currentPageNoName + ", pageSize="
				+ pageSize + ", maxRecordNum=" + maxRecordNum
				+ ", dataRegionRegex=" + dataRegionRegex + ", jobTitleRegex="
				+ jobTitleRegex + ", jobHrefRegex=" + jobHrefRegex
				+ ", jobDateRegex=" + jobDateRegex + ", jobDatePattern="
				+ jobDatePattern + ", jobCityRegex=" + jobCityRegex
				+ ", jobTypeName=" + jobTypeName + ", industryName="
				+ industryName + ", cityName=" + cityName
				+ ", companyNameRegex=" + companyNameRegex
				+ ", companyHrefRegex=" + companyHrefRegex + "]";
	}

}

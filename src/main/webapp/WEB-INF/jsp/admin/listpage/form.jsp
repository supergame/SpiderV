<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<title>管理搜索列表页配置</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/listpage"/>" method="post">
	<input type="hidden" name="id" value="${listPageConfig.id}"/>
	<table>
		<tr>
			<td colspan="4" class="tableHeadBg">
				<div align="center">管理搜索列表页配置(<span style="color: red;">*每一个搜索引擎有且只有一个列表配置</span>)</div>
			</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel" rowspan="13">页面数据相关:</td>
			<td class="tdlabel">所属搜索引擎(*):</td>
			<td>
				<select name="searchEngineId">
					<c:forEach var="engine" items="${engines}">
						<c:choose>
							<c:when test="${listPageConfig.searchEngineId == engine.id}">
								<option value="${engine.id}" selected="selected">${engine.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${engine.id}">${engine.name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">没有数据页面-正则(*):</td>
			<td>
				<input type="text" class="InputCommon" name="noDataPageRegex" size="70" value="<c:out value="${listPageConfig.noDataPageRegex}" escapeXml="true"/>"/>
			</td>
			<td>使用：Regex.isContains(R, html)判断。</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">数据区域-正则:</td>
			<td>
				<input type="text" class="InputCommon" name="dataRegionRegex" size="70" value="<c:out value="${listPageConfig.dataRegionRegex}" escapeXml="true"/>"/>
			</td>
			<td>
				如果不填写，返回HTML页面全部；<br>
				使用：Regex.matchSRowSField(htmlSource, R, false)获取。
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">记录标题-正则:</td>
			<td>
				<input type="text" class="InputCommon" name="jobTitleRegex" size="70" value="<c:out value="${listPageConfig.jobTitleRegex}" escapeXml="true"/>"/>
			</td>
			<td>使用：Regex.matchMRowSField(dataRegionHtml, R, false)获取</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">记录链接-正则:</td>
			<td>
				<input type="text" class="InputCommon" name="jobHrefRegex" size="70" value="<c:out value="${listPageConfig.jobHrefRegex}" escapeXml="true"/>"/>
			</td>
			<td>使用：Regex.matchMRowSField(dataRegionHtml, R, false)获取</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">发布记录公司-正则:</td>
			<td>
				<input type="text" class="InputCommon" name="companyNameRegex" size="70" value="<c:out value="${listPageConfig.companyNameRegex}" escapeXml="true"/>"/>
			</td>
			<td>使用：Regex.matchMRowSField(dataRegionHtml, R, false)获取</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">公司链接-正则:</td>
			<td>
				<input type="text" class="InputCommon" name="companyHrefRegex" size="70" value="<c:out value="${listPageConfig.companyHrefRegex}" escapeXml="true"/>"/>
			</td>
			<td>使用：Regex.matchMRowSField(dataRegionHtml, R, false)获取</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">发布城市-正则:</td>
			<td>
				<input type="text" class="InputCommon" name="jobCityRegex" size="70" value="<c:out value="${listPageConfig.jobCityRegex}" escapeXml="true"/>"/>
			</td>
			<td>使用：Regex.matchMRowSField(dataRegionHtml, R, false)获取</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">发布日期-正则:</td>
			<td>
				<input type="text" class="InputCommon" name="jobDateRegex" size="70" value="<c:out value="${listPageConfig.jobDateRegex}" escapeXml="true"/>"/>
			</td>
			<td>使用：Regex.matchMRowSField(dataRegionHtml, R, false)获取</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">日期转换模式(*):</td>
			<td>
				<input type="text" class="InputCommon" name="jobDatePattern" size="15" value="${listPageConfig.jobDatePattern}"/>
			</td>
			<td>JAVA中的日期转换模式：例如："yyyy-MM-dd".如果可能包含多个用“，”分割。系统将顺序尝试。</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">参数名1:</td>
			<td>
				<input type="text" class="InputCommon" name="jobTypeName" size="70" value="<c:out value="${listPageConfig.jobTypeName}" escapeXml="true"/>"/>
			</td>
			<td>从提交URL中获取，并保存到数据中</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">参数名2:</td>
			<td>
				<input type="text" class="InputCommon" name="industryName" size="70" value="<c:out value="${listPageConfig.industryName}" escapeXml="true"/>"/>
			</td>
			<td>从提交URL中获取，并保存到数据中</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">参数名3:</td>
			<td>
				<input type="text" class="InputCommon" name="cityName" size="70" value="<c:out value="${listPageConfig.cityName}" escapeXml="true"/>"/>
			</td>
			<td>从提交URL中获取，并保存到数据中</td>
		</tr>
		<!-- record and turn page -->
		<tr class="tableAlternationBg1">
			<td class="tdlabel" rowspan="5">记录数和翻页相关:</td>
			<td class="tdlabel">最大返回记录数:</td>
			<td>
				<input type="text" class="InputCommon" name="maxRecordNum" size="7" value="${listPageConfig.maxRecordNum}"/>
			</td>
			<td>如果是小于等于0，网站对返回记录数没有限制。</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">实际返回记录数-正则(*):</td>
			<td>
				<input type="text" class="InputCommon" name="returnRecordNumRegex" size="70" value="<c:out value="${listPageConfig.returnRecordNumRegex}" escapeXml="true"/>"/>
			</td>
			<td>使用：Regex.matchSRowSField(htmlSource, R, false)获取</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">每页记录显示数量(*):</td>
			<td>
				<input type="text" class="InputCommon" name="pageSize" size="4" value="${listPageConfig.pageSize}"/>
			</td>
			<td>例如："20"</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">当前页数参数名(*):</td>
			<td>
				<input type="text" class="InputCommon" name="currentPageNoName" size="70" value="<c:out value="${listPageConfig.currentPageNoName}" escapeXml="true"/>"/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<ul>
	<li>如果配置并实现了数据抽取逻辑，则抽取相关的配置可以为空。</li>
</ul>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>

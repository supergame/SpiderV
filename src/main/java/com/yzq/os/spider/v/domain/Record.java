package com.yzq.os.spider.v.domain;

import java.util.Date;

import org.apache.commons.lang.time.DateFormatUtils;

import com.yzq.os.spider.v.Constants;

/**
 * 抓取到的列表数据中每一条
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public class Record {

	/**
	 * 等待后续处理
	 */
	public static final int DO_FLAG = 0;
	
	/**
	 * 处理中
	 */
	public static final int DOING_FLAG = 1;
	
	/**
	 * 处理完毕
	 */
	public static final int DONE_FLAG = 2;

	private Integer id;
	/**
	 * 网站ID
	 */
	private int websiteId;
	
	/**
	 * 搜索引擎ID
	 */
	private int searchEngineId;
	/**
	 * 搜索urlID
	 */
	private int queryUrlId;
	
	
	private String jobTypeCode;
	private String industryCode;
	private String cityCode;
	private String jobTitle;
	private String jobLinkURL;
	private String companyName;
	private String companyLinkURL;
	private String cmpCompanyId;
	private String cityText;
	private Date jobDate;
	private String cmptrJobId;
	private int doFlag;
	private String jobSalary;
	private String jobType;
	private String uniqueMd5;

	public Record() {
		super();
		this.doFlag = 0;// Set default is 0
	}

	

	public int getWebsiteId() {
		return websiteId;
	}



	public void setWebsiteId(int websiteId) {
		this.websiteId = websiteId;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public int getSearchEngineId() {
		return searchEngineId;
	}

	public void setSearchEngineId(int searchEngineId) {
		this.searchEngineId = searchEngineId;
	}

	public int getQueryUrlId() {
		return queryUrlId;
	}

	public void setQueryUrlId(int queryUrlId) {
		this.queryUrlId = queryUrlId;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCityText() {
		return cityText;
	}

	public void setCityText(String cityText) {
		this.cityText = cityText;
	}

	public Date getJobDate() {
		return jobDate;
	}

	public void setJobDate(Date jobDate) {
		this.jobDate = jobDate;
	}

	public String getJobTypeCode() {
		return jobTypeCode;
	}

	public void setJobTypeCode(String jobTypeCode) {
		this.jobTypeCode = jobTypeCode;
	}

	public String getIndustryCode() {
		return industryCode;
	}

	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getJobLinkURL() {
		return jobLinkURL;
	}

	public void setJobLinkURL(String jobLinkURL) {
		this.jobLinkURL = jobLinkURL;
	}

	public String getCmpCompanyId() {
		return cmpCompanyId;
	}

	public void setCmpCompanyId(String cmpCompanyId) {
		this.cmpCompanyId = cmpCompanyId;
	}

	public String getCompanyLinkURL() {
		return companyLinkURL;
	}

	public void setCompanyLinkURL(String companyLinkURL) {
		this.companyLinkURL = companyLinkURL;
	}

	public int getDoFlag() {
		return doFlag;
	}

	public void setDoFlag(int doFlag) {
		this.doFlag = doFlag;
	}

	public String getUniqueMd5() {
		return uniqueMd5;
	}

	public void setUniqueMd5(String uniqueMd5) {
		this.uniqueMd5 = uniqueMd5;
	}

	public String getCmptrJobId() {
		return cmptrJobId;
	}

	public void setCmptrJobId(String cmptrJobId) {
		this.cmptrJobId = cmptrJobId;
	}

	public String getJobSalary() {
		return jobSalary;
	}

	public void setJobSalary(String jobSalary) {
		this.jobSalary = jobSalary;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	@Override
	public String toString() {
		return "Record [websiteId=" + websiteId + ", companyName=" + companyName + ", jobTitle=" + jobTitle + ", cityText=" + cityText + ", jobDate=" + (jobDate != null ? DateFormatUtils.format(jobDate, Constants.DATE_PATTERN) : null) + ", companyLinkURL=" + companyLinkURL + ", jobLinkURL=" + jobLinkURL + ", queryUrlId=" + queryUrlId + ", jobTypeCode=" + jobTypeCode
				+ ", industryCode=" + industryCode + ", cityCode=" + cityCode + ", cmpCompanyId=" + cmpCompanyId + ", cmptrJobId=" + cmptrJobId + ", doFlag=" + doFlag + ", jobSalary=" + jobSalary + ", jobType=" + jobType + ", uniqueMd5=" + uniqueMd5 + ", searchEngineId=" + searchEngineId + ", id=" + id + "]";
	}

}

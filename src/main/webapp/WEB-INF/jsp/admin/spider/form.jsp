<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>执行数据抓取</title>
<script type="text/javascript">
<!--
	$(document).ready(function() {
		//button ui
		$("#datepicker").datepicker();
		$("#datepicker").datepicker("option", "dateFormat", 'yy-mm-dd' );
	});
//-->
</script>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/crawl"/>" method="post">
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">执行数据抓取</div>
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">运行服务器:</td>
			<td>
				<select name="ip">
					<c:forEach var="server" items="${servers}">
						<option value="${server.ip}">${server.ip}</option>
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">搜索引擎:</td>
			<td>
				<select name="searchEngineId">
					<c:forEach var="engine" items="${engines}">
						<option value="${engine.id}">${engine.name}</option>
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">日期:</td>
			<td>
				<input type="text" class="InputCommon" id="datepicker" name="date" />
			</td>
			<td><span style="color: red;">指定日期的数据存储表将被创建，默认为当天日期，线程池的核心线程为1，最大线程为3</span></td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">每次加载queryURL数量:</td>
			<td>
				<input type="text" class="InputCommon" name="findUrlSize" value="50" />
			</td>
			<td><span style="color: red;">每次获取queryurl数量</span></td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">线程池最小线程数:</td>
			<td>
				<input type="text" class="InputCommon" name="minThreadNum" value="2" />
			</td>
			<td><span style="color: red;">核心线程数量</span></td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">线程池最大线程数量:</td>
			<td>
				<input type="text" class="InputCommon" name="maxThreadNum" value="10" />
			</td>
			<td><span style="color: red;">最大线程数量，每个网站抓取任务将单独创建执行线程池，不可太大</span></td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">执行任务线程池队列长度:</td>
			<td>
				<input type="text" class="InputCommon" name="arrayQueueSize" value="1000" />
			</td>
			<td><span style="color: red;">ThreadPoolExecutor arraylinkedQueue size</span></td>
		</tr>
		<tr class="tableAlternationBg2">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>

package com.yzq.os.spider.v.util;
/**
 * 编码枚举类
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public enum Encode {
	GB18030("GB18030", "GB18030"), 
	GBK("GBK", "GBK"), 
	GB2312("GB2312", "GB2312"), 
	UTF8("UTF8", "UTF-8"), 
	ISO88591("ISO88591", "ISO-8859-1");
	
	private String key;
	private String encode;

	private Encode(String key, String encode) {
		this.key = key;
		this.encode = encode;
	}

	public static Encode getEncode(String encode) {
		Encode returnValue = null;
		Encode[] encodes = values();
		for (Encode code : encodes) {
			if (code.getEncode().equalsIgnoreCase(encode)) {
				returnValue = code;
				break;
			}
		}
		if (returnValue == null) {
			throw new IllegalArgumentException();
		}
		return returnValue;
	}

	public String getKey() {
		return key;
	}

	public String getEncode() {
		return encode;
	}
}

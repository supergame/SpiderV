package com.yzq.os.spider.v.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.yzq.os.spider.v.dao.mapper.SearchEngineParamMapper;
import com.yzq.os.spider.v.domain.SearchEngineParam;

@Repository
public class SearchEngineParamDao extends AbstractDao {

	public void save(SearchEngineParam param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" insert into search_engine_param ");
		sql.append("   (search_engine_id, ");
		sql.append("    name, ");
		sql.append("    value, ");
		sql.append("    single_value, ");
		sql.append("    required, ");
		sql.append("    order_num, ");
		sql.append("    describe_txt) ");
		sql.append(" values ");
		sql.append("   (?, ?, ?, ?, ?, ?, ?) ");

		List<Object> paramList = new ArrayList<Object>();
		paramList.add(param.getSearchEngineId());
		paramList.add(param.getName());
		paramList.add(param.getValue());
		paramList.add(param.getSingleValue());
		paramList.add(param.getRequired());
		paramList.add(param.getOrderNum());
		paramList.add(param.getDesc());

		jdbcTemplateOnline.update(sql.toString(), paramList.toArray());
	}

	public List<SearchEngineParam> findBySearchEngineId(int searchEngineId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select t.id, ");
		sql.append("        t.search_engine_id, ");
		sql.append("        t.name, ");
		sql.append("        t.value, ");
		sql.append("        t.single_value, ");
		sql.append("        t.required, ");
		sql.append("        t.order_num, ");
		sql.append("        t.describe_txt ");
		sql.append("   from search_engine_param t ");
		sql.append("  where t.search_engine_id = ? ");
		sql.append("  order by t.single_value desc, t.required desc, t.order_num ");

		return jdbcTemplateOnline.query(sql.toString(), new Object[] { searchEngineId }, new SearchEngineParamMapper());
	}

	public List<SearchEngineParam> findList(int searchEngineId, int singleValue, int required) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select t.id, ");
		sql.append("        t.search_engine_id, ");
		sql.append("        t.name, ");
		sql.append("        t.value, ");
		sql.append("        t.single_value, ");
		sql.append("        t.required, ");
		sql.append("        t.order_num, ");
		sql.append("        t.describe_txt ");
		sql.append("   from search_engine_param t ");
		sql.append("  where t.search_engine_id = ? ");
		sql.append("    and t.single_value = ? ");
		sql.append("    and t.required = ? ");
		sql.append("  order by t.order_num ");

		return jdbcTemplateOnline.query(sql.toString(), new Object[] { searchEngineId, singleValue, required }, new SearchEngineParamMapper());
	}

	public SearchEngineParam findByPkId(int searchEngineParamId) {
		StringBuffer sql = new StringBuffer();
		sql.append(" select t.id, ");
		sql.append("        t.search_engine_id, ");
		sql.append("        t.name, ");
		sql.append("        t.value, ");
		sql.append("        t.single_value, ");
		sql.append("        t.required, ");
		sql.append("        t.order_num, ");
		sql.append("        t.describe_txt ");
		sql.append("   from search_engine_param t ");
		sql.append("  where t.id = ? ");

		return jdbcTemplateOnline.queryForObject(sql.toString(), new Object[] { searchEngineParamId }, new SearchEngineParamMapper());
	}

	public void update(SearchEngineParam param) {
		StringBuffer sql = new StringBuffer();
		sql.append(" update search_engine_param t ");
		sql.append("    set t.search_engine_id = ?, ");
		sql.append("        t.name             = ?, ");
		sql.append("        t.value            = ?, ");
		sql.append("        t.single_value     = ?, ");
		sql.append("        t.required         = ?, ");
		sql.append("        t.order_num        = ?, ");
		sql.append("        t.describe_txt     = ? ");
		sql.append("  where t.id = ? ");

		List<Object> paramList = new ArrayList<Object>();
		paramList.add(param.getSearchEngineId());
		paramList.add(param.getName());
		paramList.add(param.getValue());
		paramList.add(param.getSingleValue());
		paramList.add(param.getRequired());
		paramList.add(param.getOrderNum());
		paramList.add(param.getDesc());
		paramList.add(param.getId());

		jdbcTemplateOnline.update(sql.toString(), paramList.toArray());
	}

	public void deleteBySearchEngineId(Integer searchEngineId) {
		String sql = " delete from search_engine_param where search_engine_id = ? ";
		jdbcTemplateOnline.update(sql, searchEngineId);
	}
}

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<link href="<c:url value="/css/ui-lightness/jquery-ui-1.8.6.custom.css"/>" rel="stylesheet" type="text/css"  />
<script src="<c:url value="/js/jquery-1.4.2.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/js/jquery-ui-1.8.6.custom.min.js"/>" type="text/javascript"></script>
<title>表“${tableName}”的前${returnSize}条记录   总数:${totalCount}</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<table>
	<tr>
		<td colspan="18" class="tableHeadBg">
			<div align="center">表“${tableName}”的前${returnSize}条记录   总数:${totalCount}</div>
		</td>
	</tr>
	<tr class="tableTitleBg">
		<td>id</td>
		<td>search_engine_id</td>
		<td>query_url_id</td>
		<td>cmptr_job_id</td>
		<td>job_title</td>
		<td>cmptr_company_id</td>
		<td>company_name</td>
		<td>city_text</td>
		<td>job_date</td>
		<td>job_type_code</td>
		<td>job_industry_code</td>
		<td>job_city_code</td>
		<td>job_salary</td>
		<td>job_type</td>
		<td>job_link_url</td>
		<td>company_link_url</td>
		<td>unique_md5</td>
		<td>do_flag</td>
	</tr>
	<c:forEach items="${jobs}" var="job" varStatus="status" >
	<tr class="<c:if test="${status.count%2==0}">tableAlternationBg2</c:if><c:if test="${status.count%2==1}">tableAlternationBg1</c:if>">
		<td>${job.id}</td>
		<td>${job.searchEngineId}</td>
		<td>${job.queryUrlId}</td>
		<td>${job.cmptrJobId}</td>
		<td>${job.jobTitle}</td>
		<td>${job.cmpCompanyId}</td>
		<td>${job.companyName}</td>
		<td>${job.cityText}</td>
		<td>${job.jobDate}</td>
		<td>${job.jobTypeCode}</td>
		<td>${job.industryCode}</td>
		<td>${job.cityCode}</td>
		<td>${job.jobSalary}</td>
		<td>${job.jobType}</td>
		<td>${job.jobLinkURL}</td>
		<td>${job.companyLinkURL}</td>
		<td>${job.uniqueMd5}</td>
		<td>${job.doFlag}</td>
	</tr>
	</c:forEach>
</table>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet"
	type="text/css" />
<title>搜索引擎参数列表</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
<table>
	<tr>
		<td colspan="7" class="tableHeadBg">
			<div align="center">搜索引擎[${searchEngine.name }]的参数列表</div>
		</td>
	</tr>
	<tr class="tableTitleBg">
		<td width="15%">参数名</td>
		<td width="15%">值</td>
		<td width="10%">是否单值</td>
		<td width="10%">
		是否必须<br>
		必须：初始化生成URL必须含有。<br>
		不必须：初始生成不包含，用作限定条件。</td>
		<td width="15%">排序值</td>
		<td width="15%">描述</td>
		<td width="20%">操作</td>
	</tr>
	<c:forEach items="${searchEngineParams}" var="searchEngineParam" varStatus="status" >
	<tr class="<c:if test="${status.count%2==0}">tableAlternationBg2</c:if><c:if test="${status.count%2==1}">tableAlternationBg1</c:if>">
		<td>${searchEngineParam.name }</td>
		<td>${fn:length(searchEngineParam.value)>21 ? fn:substring(searchEngineParam.value,0,21) : searchEngineParam.value}</td>
		<td>
			<c:if test="${searchEngineParam.singleValue == 1 }">
				<span style="color: green;">单值</span>
			</c:if>
			<c:if test="${searchEngineParam.singleValue == 0 }">
				<span style="color: red;">多值列表</span>
			</c:if>
		</td>
		<td>
			<c:if test="${searchEngineParam.required == 1 }">
				<span style="color: red;">必须</span>
			</c:if>
			<c:if test="${searchEngineParam.required == 0 }">
				<span style="color: green;">不必须</span>
			</c:if>
		</td>
		<td>${searchEngineParam.orderNum }</td>
		<td>${searchEngineParam.desc }</td>
		<td><a href="${pageContext.request.contextPath}/admin/param/modify/${searchEngineParam.id }">修改</a></td>
	</tr>
	</c:forEach>
</table>
<br>
<a href="<c:url value="/index.jsp" />">返回首页</a>
<br>
</body>
</html>

package com.yzq.os.spider.v.service.spider;

import java.util.Date;

import com.yzq.os.spider.v.domain.SearchEngine;
import com.yzq.os.spider.v.service.domain.SpiderRecordService;
import com.yzq.os.spider.v.service.domain.QueryURLService;

/**
 * 数据补全,通过标准的方案执行数据的获取,当获取数据有遗漏的时候,通过此接口实现遗漏数据的补全，通过某种规则生成新的搜索URL,并保存
 * 
 * @author DEV-MARTIN
 * 
 */
public interface CompletionCrawl {

	void setCrawlJobService(SpiderRecordService crawlJobService);

	void setSearchEngineId(int searchEngineId);

	void setMarkDate(Date markDate);

	void setJobSaveTableName(String jobSaveTableName);

	void setSearchEngine(SearchEngine searchEngine);

	void setQueryURLService(QueryURLService queryURLService);
	
	void doCompletion() throws Exception;
}

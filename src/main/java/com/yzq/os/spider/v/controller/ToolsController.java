package com.yzq.os.spider.v.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.yzq.os.spider.v.Constants;
import com.yzq.os.spider.v.service.http.HttpClientService;

/**
 * 应用小工具集合
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
@Controller
@RequestMapping("/tools")
public class ToolsController {

	/**
	 * 页面抓取模拟
	 * 
	 * @return
	 */
	@RequestMapping("/form2")
	public ModelAndView form2() {
		return new ModelAndView("/admin/tools/form2");
	}

	/**
	 * 执行页面抓取模拟
	 * 
	 * @param url
	 * @param isGzip
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/html", method = RequestMethod.POST)
	public ModelAndView getHtml(String url, int isGzip) throws Exception {
		// url="http://searchjob.chinahr.com/Africa-Sales--searchresult.html?occIDList=100&occParentIDList=100&myLocIDList=42000&myLocParentIDList=42000&indIDList=1126000&isInterView=1&IsModel=false&from=search&prj=quick";
		HttpClientService httpClientService = Constants.getApplicationContext()
				.getBean("httpClientService", HttpClientService.class);
		boolean _isGzip = isGzip == 1 ? true : false;
		String htmlSource = httpClientService.doGetRequest(url, _isGzip);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("url", url);
		model.put("isGzip", isGzip);
		model.put("htmlSource", htmlSource);
		return new ModelAndView("/admin/tools/form2", model);
	}

}

package com.yzq.os.spider.v.service.addition.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.yzq.os.spider.v.service.addition.AbstractLogin;
import com.yzq.os.spider.v.util.Encode;

/**
 * 自动登录实现
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
public class DemoLogin extends AbstractLogin {

	private static final String SIGNIN_INDEX = "http://www.xxx.com/my/login.php";
	private static final String LOGIN_USERNAME = "aaa";
	private static final String LOGIN_PASSWORD = "bbb";

	@Override
	public void login() throws Exception {
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("name", LOGIN_USERNAME));
		parameters.add(new BasicNameValuePair("pwd", LOGIN_PASSWORD));
		httpClientService.doPostRequest(SIGNIN_INDEX, Encode.UTF8, true, null,
				parameters, "UTF-8");
	}

}

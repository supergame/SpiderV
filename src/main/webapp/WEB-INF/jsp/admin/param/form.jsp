<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<title>管理搜索引擎参数</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/param"/>" method="post">
	<input type="hidden" name="id" value="${searchEngineParam.id}"/>
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">管理搜索引擎参数</div>
			</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">所属搜索引擎:</td>
			<td>
				<select name="searchEngineId">
					<c:forEach var="engine" items="${engines}">
						<c:choose>
							<c:when test="${searchEngineParam.searchEngineId == engine.id}">
								<option value="${engine.id}" selected="selected">${engine.name}</option>
							</c:when>
							<c:otherwise>
								<option value="${engine.id}">${engine.name}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">参数名:</td>
			<td>
				<input type="text" class="InputCommon" name="name" value="${searchEngineParam.name}"/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">参数值:</td>
			<td>
				<textarea class="InputCommon" rows="15" cols="70" name="value">${searchEngineParam.value}</textarea>
			</td>
			<td>
				可以每行只有code，也可以每行有code和name形式。参数值中的空格和换行符号将被忽略。<br>
				例如：“01,计算机软件”。注意code必须在前面，中间用英文逗号","分隔。如果需要转义，用转义语法：<span style="color: red;">\*,*\</span>
			</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">是否单值:</td>
			<td>
				是：<input type="radio" name="singleValue" value="1" <c:if test="${searchEngineParam == null || searchEngineParam.singleValue == 1}">checked="checked"</c:if> />
				否：<input type="radio" name="singleValue" value="0" <c:if test="${searchEngineParam.singleValue == 0}">checked="checked"</c:if> />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">是否必须:</td>
			<td>
				是：<input type="radio" name="required" value="1" <c:if test="${searchEngineParam == null || searchEngineParam.required == 1}">checked="checked"</c:if> />
				否：<input type="radio" name="required" value="0" <c:if test="${searchEngineParam.required == 0}">checked="checked"</c:if> />
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel">排序值:</td>
			<td>
				<input type="text" class="InputCommon" name="orderNum" value="${searchEngineParam.orderNum}"/>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg1">
			<td class="tdlabel">描述:</td>
			<td>
				<textarea class="InputCommon" rows="5" cols="70" name="desc">${searchEngineParam.desc}</textarea>
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<ul>
	<li>必须添加分页参数，并且分页参数值设置为第一页</li>
	<li>可选占位符号:</li>
	<c:forEach items="${placeHolders }" var="placeHolder">
	     <li><c:out value="${placeHolder }" escapeXml="true"/></li>
    </c:forEach>
</ul>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>

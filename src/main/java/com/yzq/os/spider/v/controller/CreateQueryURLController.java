package com.yzq.os.spider.v.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.yzq.os.spider.v.domain.QueryURL;
import com.yzq.os.spider.v.domain.SearchEngine;
import com.yzq.os.spider.v.service.domain.QueryURLService;
import com.yzq.os.spider.v.service.domain.SearchEngineService;
import com.yzq.os.spider.v.service.queryurl.CreateQueryURL;
import com.yzq.os.spider.v.util.EncodeUtil;

/**
 * 搜索URL控制器
 * 
 * @author 苑志强(xingyu_yzq@163.com)
 * 
 */
@Controller
@RequestMapping("/createurl")
public class CreateQueryURLController {

	private static Logger logger = Logger
			.getLogger(CreateQueryURLController.class);

	@Autowired
	private SearchEngineService searchEngineService;

	@Autowired
	private QueryURLService queryURLService;

	/**
	 * 搜索URL创建页面
	 * 
	 * @return
	 */
	@RequestMapping("/form")
	public ModelAndView form() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		return new ModelAndView("/admin/createurl/form", model);
	}

	/**
	 * 从备份表中加载搜索URL页面
	 * 
	 * @return
	 */
	@RequestMapping("/formBack")
	public ModelAndView formBack() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		return new ModelAndView("/admin/createurl/formBack", model);
	}

	/**
	 * 手动插入搜索URL页面
	 * 
	 * @return
	 */
	@RequestMapping("/insert")
	public ModelAndView insertForm() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("engines", searchEngineService.findUIViews());
		return new ModelAndView("/admin/createurl/insertForm", model);
	}

	/**
	 * 手动插入搜索URL到备份表页面
	 * 
	 * @param searchEngineId
	 * @return
	 */
	@RequestMapping(value = "/insertBack", method = RequestMethod.POST)
	public RedirectView insertBack(int searchEngineId) {
		queryURLService.batchSaveToBackTable(searchEngineId);
		return new RedirectView("formBack");
	}

	/**
	 * 执行手动搜索URL插入到备份表
	 * 
	 * @param searchEngineId
	 * @param urlStrs
	 * @return
	 */
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public RedirectView insertPost(int searchEngineId, String urlStrs) {
		if (StringUtils.isNotBlank(urlStrs)) {
			String[] urls = StringUtils.split(urlStrs, "\n");
			if (ArrayUtils.isNotEmpty(urls)) {
				trimStringArr(urls);// trim
				queryURLService.batchSaveToBackTable(searchEngineId, urls);
				logger.info("Insert into back table for engine["
						+ searchEngineId + "],size:["
						+ ArrayUtils.getLength(urls) + "].");
			}
		}
		return new RedirectView("insert");
	}

	/**
	 * 将数组中的每个元素去空格
	 * 
	 * @param input
	 */
	private void trimStringArr(String[] input) {
		for (int i = 0; i < input.length; i++) {
			input[i] = StringUtils.trim(input[i]);
		}
	}

	/**
	 * 创建搜索URL,找到搜索引擎和对应的搜索引擎参数，通过组合方式获取全部可能的URL,并每次3000条的数量插入搜索URL表中
	 * 
	 * @param searchEngineId
	 * @return
	 * @throws ClassNotFoundException
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(method = RequestMethod.POST)
	public RedirectView createUrl(int searchEngineId)
			throws ClassNotFoundException, UnsupportedEncodingException {
		SearchEngine searchEngine = searchEngineService
				.findById(searchEngineId);
		logger.info(EncodeUtil.gbk2iso("Begin create url of engine:["
				+ searchEngine.getName() + "]."));
		StopWatch stopWatch = new StopWatch();
		CreateQueryURL createQueryURL = null;
		String createQueryURLClass = searchEngine.getCreateQueryURLClass();
		if (StringUtils.isNotBlank(createQueryURLClass)) {
			createQueryURL = searchEngineService
					.instantiateQueryURLClass(createQueryURLClass);
			stopWatch.start();
			List<QueryURL> queryURLs = createQueryURL
					.createInitQueryUrls(searchEngineId);
			stopWatch.stop();
			int count = CollectionUtils.size(queryURLs);
			logger.info(EncodeUtil.gbk2iso("Instantiate url of engine:["
					+ searchEngine.getName() + "] count:[" + count
					+ "]. cost(second):[" + stopWatch.getTotalTimeSeconds()
					+ "]"));
			stopWatch.start();
			queryURLService.batchSave(searchEngineId, queryURLs, 3000);
			stopWatch.stop();
			logger.info(EncodeUtil.gbk2iso("End create url of engine:["
					+ searchEngine.getName() + "] count:[" + count
					+ "]. cost(second):[" + stopWatch.getTotalTimeSeconds()
					+ "]"));
		}
		return new RedirectView("createurl/form");
	}

}

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions"  prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="<c:url value="/css/system.css"/>" rel="stylesheet" type="text/css" />
<title>从已有配置导入搜索引擎</title>
</head>
<body>
<a href="<c:url value="/index.jsp" />">返回首页</a><br>
<form action="<c:url value="/admin/engine/import"/>" method="post">
	<table>
		<tr>
			<td colspan="3" class="tableHeadBg">
				<div align="center">从已有配置导入搜索引擎</div>
			</td>
		</tr>
		<tr class="tableAlternationBg2">
			<td class="tdlabel" width="20%">XML:</td>
			<td width="60%">
				<textarea class="InputCommon" rows="30" cols="140" name="xml"></textarea>
			</td>
			<td width="20%"><span style="color: red;">注意：如果搜索引擎ID重复，将视作更新，但是“搜索引擎参数”和“结果列表配置”将删除后从新创建</span></td>
		</tr>
		<tr class="tableAlternationBg1">
			<td>&nbsp;</td>
			<td>
				<input type="submit" value="提交"><input type="reset" value="重置">
			</td>
			<td>&nbsp;</td>
		</tr>
	</table>
</form>
<br><a href="<c:url value="/index.jsp" />">返回首页</a><br>
</body>
</html>

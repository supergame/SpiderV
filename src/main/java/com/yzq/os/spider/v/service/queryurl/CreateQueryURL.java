package com.yzq.os.spider.v.service.queryurl;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.yzq.os.spider.v.domain.QueryURL;
import com.yzq.os.spider.v.service.domain.SearchEngineParamService;
import com.yzq.os.spider.v.service.domain.SearchEngineService;

/**
 * 排列组合生成搜索URL
 * @author 苑志强(xingyu_yzq@163.com)
 *
 */
public interface CreateQueryURL {
	
	void setSearchEngineService(SearchEngineService searchEngineService);
	
	void setSearchEngineParamService(SearchEngineParamService searchEngineParamService);
	
	List<QueryURL> createInitQueryUrls(int searchEngineId) throws UnsupportedEncodingException;
	
	List<QueryURL> generateQualifiedURLs(QueryURL queryURL) throws UnsupportedEncodingException;
	
	String toPostUrl(String spellUrl);
	
	String toSpellUrl(String postUrl);
}

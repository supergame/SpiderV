-- MySQL dump 10.13  Distrib 5.5.38, for Linux (x86_64)
--
-- Host: localhost    Database: SpiderV
-- ------------------------------------------------------
-- Server version	5.5.38-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `init_query_url_bak`
--

DROP TABLE IF EXISTS `init_query_url_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `init_query_url_bak` (
  `id` int(11) NOT NULL DEFAULT '0',
  `search_engine_id` int(11) NOT NULL,
  `spell_url` varchar(2048) NOT NULL,
  `post_url` varchar(2048) NOT NULL,
  `do_flag` int(11) NOT NULL DEFAULT '0' COMMENT '0:wait\n1:doing\n2:ok\n3:error\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `list_page_config`
--

DROP TABLE IF EXISTS `list_page_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_page_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_engine_id` int(11) NOT NULL,
  `no_data_page_regex` varchar(1024) NOT NULL,
  `return_record_num_regex` varchar(1024) NOT NULL,
  `current_page_no_name` varchar(1024) NOT NULL,
  `page_size` int(11) NOT NULL,
  `max_record_num` int(11) NOT NULL DEFAULT '0',
  `data_region_regex` varchar(1024) NOT NULL,
  `job_title_regex` varchar(1024) NOT NULL,
  `job_href_regex` varchar(1024) NOT NULL,
  `job_date_regex` varchar(1024) NOT NULL,
  `job_date_pattern` varchar(45) NOT NULL,
  `job_city_regex` varchar(1024) NOT NULL,
  `job_type_name` varchar(45) NOT NULL,
  `job_industry_name` varchar(45) NOT NULL,
  `company_name_regex` varchar(1024) NOT NULL,
  `company_href_regex` varchar(1024) NOT NULL,
  `job_city_name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_engine`
--

DROP TABLE IF EXISTS `search_engine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_engine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `method` int(11) NOT NULL COMMENT '0:get,1:post',
  `base_url` varchar(512) NOT NULL,
  `is_gzip` int(1) NOT NULL DEFAULT '1',
  `encode` int(11) NOT NULL COMMENT '0:GB18030\n1:GB2312\n2:GBK\n3:UTF-8\n4:ISO-8859-1',
  `url_encode` int(11) NOT NULL DEFAULT '3' COMMENT '0:gb18030\n1:gb2312\n2:gbk\n3:utf-8\n4:iso-8859-1',
  `sleep_time` int(11) NOT NULL,
  `create_query_url_class` varchar(256) NOT NULL,
  `before_spider_processor_class` varchar(256) DEFAULT NULL,
  `spider_task_class` varchar(256) NOT NULL,
  `completion_spider_class` varchar(256) DEFAULT NULL,
  `login_class` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `search_engine_param`
--

DROP TABLE IF EXISTS `search_engine_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `search_engine_param` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `search_engine_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `value` text NOT NULL,
  `single_value` int(11) NOT NULL COMMENT '0:no\n1:yes',
  `required` int(11) NOT NULL COMMENT '0:no\n1:yes',
  `order_num` int(11) NOT NULL DEFAULT '0',
  `describe_txt` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `servers`
--

DROP TABLE IF EXISTS `servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(45) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '1:master,0:slave',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spider_run_log`
--

DROP TABLE IF EXISTS `spider_run_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spider_run_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `spider_date` date NOT NULL,
  `search_engine_id` int(11) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `query_count` int(11) NOT NULL,
  `query_speed` int(11) NOT NULL,
  `record_count` int(11) NOT NULL,
  `record_speed` int(11) NOT NULL,
  `finish_time` datetime NOT NULL,
  `start_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `spider_date` (`spider_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `spider_task`
--

DROP TABLE IF EXISTS `spider_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `spider_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `run_server_ip` varchar(45) NOT NULL,
  `search_engine_id` int(11) NOT NULL,
  `find_url_size` int(11) NOT NULL,
  `min_thread_num` int(11) NOT NULL,
  `max_thread_num` int(11) NOT NULL,
  `pool_queue_size` int(11) NOT NULL,
  `cron_exp` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-08 15:35:50
